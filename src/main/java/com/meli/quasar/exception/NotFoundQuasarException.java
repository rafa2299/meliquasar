package com.meli.quasar.exception;

import org.springframework.http.HttpStatus;

public class NotFoundQuasarException extends QuasarException {
    public NotFoundQuasarException(String message) {
        super(message);
        this.httpStatus = HttpStatus.NOT_FOUND;
    }

    public NotFoundQuasarException(String message, Throwable cause) {
        super(message, cause);
        this.httpStatus = HttpStatus.NOT_FOUND;
    }
}
