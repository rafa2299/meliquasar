package com.meli.quasar.exception;

import org.springframework.http.HttpStatus;

public class QuasarException extends Exception {

    protected HttpStatus httpStatus;

    public QuasarException(String message) {
        super(message);
    }

    public QuasarException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
