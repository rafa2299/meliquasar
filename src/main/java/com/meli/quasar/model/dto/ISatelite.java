package com.meli.quasar.model.dto;

import java.math.BigDecimal;
import java.util.List;

public interface ISatelite {
    BigDecimal getDistance();

    List<String> getMessage();
}
