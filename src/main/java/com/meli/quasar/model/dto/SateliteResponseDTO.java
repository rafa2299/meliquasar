package com.meli.quasar.model.dto;

public class SateliteResponseDTO {
    private PositionDTO position;
    private String message;

    public PositionDTO getPosition() {
        return position;
    }

    public void setPosition(PositionDTO position) {
        this.position = position;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
