package com.meli.quasar.model.dto;

import java.math.BigDecimal;
import java.util.List;

public class SateliteSplitDTO implements ISatelite {

    private BigDecimal distance;
    private List<String> message;

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }
}
