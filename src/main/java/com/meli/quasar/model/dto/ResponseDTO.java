package com.meli.quasar.model.dto;

public class ResponseDTO {
    private Integer code;
    private String response;

    public ResponseDTO(Integer code, String response) {
        this.code = code;
        this.response = response;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
