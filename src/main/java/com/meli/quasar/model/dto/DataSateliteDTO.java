package com.meli.quasar.model.dto;

import java.math.BigDecimal;
import java.util.List;

public class DataSateliteDTO implements ISatelite {
    private String name;
    private BigDecimal distance;
    private List<String> message;

    public DataSateliteDTO(String name, BigDecimal distance, List<String> message) {
        this.name = name;
        this.distance = distance;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }
}
