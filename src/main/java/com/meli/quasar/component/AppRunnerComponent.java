package com.meli.quasar.component;

import com.meli.quasar.model.Satelite;
import com.meli.quasar.repository.SateliteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class AppRunnerComponent implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppRunnerComponent.class);
    @Autowired
    private SateliteRepository sateliteRepository;

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Begin updating data, command runner");
        Satelite kenobi = new Satelite();
        kenobi.setName("kenobi");
        this.sateliteRepository.save(kenobi);
        Satelite sato = new Satelite();
        sato.setName("sato");
        this.sateliteRepository.save(sato);
        Satelite skywalker = new Satelite();
        skywalker.setName("skywalker");
        this.sateliteRepository.save(skywalker);
    }

}