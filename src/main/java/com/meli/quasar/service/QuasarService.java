package com.meli.quasar.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.meli.quasar.exception.BadRequestQuasarException;
import com.meli.quasar.exception.QuasarException;
import com.meli.quasar.model.dto.DataSateliteDTO;
import com.meli.quasar.model.dto.PositionDTO;
import com.meli.quasar.model.dto.SateliteResponseDTO;
import com.meli.quasar.model.dto.SateliteSplitDTO;

import java.util.List;

public interface QuasarService {
    SateliteSplitDTO saveDataSatelite(String name, SateliteSplitDTO sateliteSplitRequestDTO) throws JsonProcessingException, QuasarException;

    SateliteResponseDTO getDataSatelite() throws BadRequestQuasarException;

    SateliteResponseDTO getPositionAndMessage(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException;

    PositionDTO getLocation(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException;

    String getMessage(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException;
}
