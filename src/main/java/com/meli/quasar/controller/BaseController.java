package com.meli.quasar.controller;

import com.meli.quasar.model.dto.ResponseDTO;

public abstract class BaseController {
    public ResponseDTO getResponseOk(String response) {
        return new ResponseDTO(200, response);
    }
}
