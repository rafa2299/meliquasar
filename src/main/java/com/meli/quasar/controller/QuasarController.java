package com.meli.quasar.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.meli.quasar.exception.BadRequestQuasarException;
import com.meli.quasar.exception.QuasarException;
import com.meli.quasar.model.dto.SateliteRequestDTO;
import com.meli.quasar.model.dto.SateliteResponseDTO;
import com.meli.quasar.model.dto.SateliteSplitDTO;
import com.meli.quasar.service.QuasarService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("quasar")
public class QuasarController extends BaseController {

    @Autowired
    QuasarService quasarService;

    @ApiOperation(value = "Returns position and message", response = SateliteResponseDTO.class, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Internal error server")
    })
    @PostMapping(value = "topsecret", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SateliteResponseDTO> test(@RequestBody SateliteRequestDTO sateliteRequestDTO) throws QuasarException {
        return ResponseEntity.ok(this.quasarService.getPositionAndMessage(sateliteRequestDTO.getSatellites()));
    }

    @ApiOperation(value = "Save data in satelite db", response = SateliteSplitDTO.class, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Internal error server")
    })
    @PostMapping(value = "topsecret_split/{name}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SateliteSplitDTO> postTopSecretSplit(@PathVariable String name, @RequestBody SateliteSplitDTO sateliteSplitRequestDTO) throws JsonProcessingException, QuasarException {
        return ResponseEntity.ok(this.quasarService.saveDataSatelite(name, sateliteSplitRequestDTO));
    }

    @ApiOperation(value = "Returns position and message, if info satelite db is complete", response = SateliteResponseDTO.class, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Internal error server")
    })
    @GetMapping(value = "topsecret_split", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SateliteResponseDTO> getTopSecretSplit() throws BadRequestQuasarException {
        return ResponseEntity.ok(this.quasarService.getDataSatelite());
    }
}
