package com.meli.quasar.repository;

import com.meli.quasar.model.Satelite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SateliteRepository extends JpaRepository<Satelite, Integer> {
    Satelite findOneByName(String name);
}
