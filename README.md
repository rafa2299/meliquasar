# MeliQuasar

## Operación fuego Quasar

Api que retorna la fuente y contenido del mensaje de auxilio. Para esto, 
se cuenta con tres satélites que permitirán triangular la posición.
El mensaje puede no llegar completo a cada satélite debido al campo de asteroides frente a la nave.


Api rest que  cuenta con tres servicios expuestos

### Nivel 1 - Nivel 2

#### Endpoint 1
```shell
/quasar/topsecret [POST]
```
Este endpoint recibe informacion de los tres satelite (distancia, nombre y mensaje), esta informacion debe estar completa, caso contrario se repondera con un error 400 con un mensaje.
Si toda la informacion esta completa devolvera la posición y el mensaje

Acepta un json como el siguiente
```json
{
    "satellites": [
        {
            "name": "kenobi",
            "distance": 100.0,
            "message": [
                "este","","","mensaje",""
            ]
        },
        {
            "name": "skywalker",
            "distance": 115.5,
            "message": [
                "","es","","","secreto"
            ]
        },
        {
            "name": "sato",
            "distance": 142.7,
            "message": [
                "este","","un","",""
            ]
        }
    ]
}
```
Y como respuesta obtenemos un json, por ejemplo
```json
{
    "position": {
        "x": 300.0,
        "y": -300.0
    },
    "message": "este es un mensaje"
}
```
### Nivel 3

#### Endpoint 1
Este endpoint, recibe informacion del satelite, lo valida y guarda en una BD temporal, para futuros calculos.
```shell
 /topsecret_split/{satellite_name} [POST]
```
Se debe enviar como pararametro de URL el nombre del satelite, este debe existir (kenobi, sato y skywalker), caso contrario devolvera un 404.
Acepta como boy request un json como el siguiente
```json
{
"distance": 400.0,
"message": ["este", "mensaje", "es", "secreto"]
}
```
#### Endpoint 2
```shell
 /topsecret_split [GET]
```
Devuelve la pocision de la nave y el mensaje, siemprey cuando, para cada satelite exista informacion necesario para realizar el calculo.
Para cada satelite debe existir cargada la distancia y el mensaje, de lo contrario devolvera un 400 o 404 segun corresponda, con el mesaje de error correspondiente.

### Swagger
Para obtener mas información y testear el servicio pude acceder al swagger

[https://thawing-journey-10473.herokuapp.com/swagger-ui.html](https://thawing-journey-10473.herokuapp.com/swagger-ui.html)

### Instalación y ejecucion
```shell
mvn clean install
```

```shell
mvn spring-boot:run
```


